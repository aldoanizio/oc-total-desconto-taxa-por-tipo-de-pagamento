﻿Módulo Desconto/Taxa Por Tipo de Pagamento
=============================

* Módulo total de pedido que permite aplicar uma taxa adicional ou desconto de acordo com a forma de pagamento

-----------
#### Versão
* 2.0

----------------
#### Tecnologias

* [PHP] - versão >= 5.3
* [OpenCart] - versão >= 2.x

------------------------------
#### Instruções de Instalação

* 1 -> Envie os arquivos da pasta 'upload' para a raiz da loja respeitando a estrutura de pastas
* 2 -> Acesse o menu 'Configurações > Usuários > Grupos de Usuários', edite os grupos e marque a permissão 'total/taxa_por_tipo_pagamento'
* 3 -> Acesse o menu 'Extensões > Total do pedido', instale e configure o módulo 'Desconto/Taxa por Tipo de Pagamento'